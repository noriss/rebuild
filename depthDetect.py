#!/usr/bin/env python

import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt

# local modules
from video import create_capture
from common import clock, draw_str
import time

if __name__ == '__main__':

    img = cv.imread('tolppa.jpg')

    gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
    temp = gray
    gray = np.float32(gray)

    corners = cv.goodFeaturesToTrack(gray, 1028, 0.01, 10)
    corners = np.int0(corners)
    vis = img.copy()
    vis = np.uint8(vis / 2.)
    edge = cv.Canny(temp, 0, 2992, apertureSize=5)
    vis[edge != 0] = (0, 255, 0)

    for i in corners:
        x,y = i.ravel()
        cv.circle(img, (x,y), 4, (0, 0, 255), -1)
    img = np.uint8(img/2.)
    img[edge != 0] = (0, 255, 0)
    cv.imshow('courner', img)
    cv.waitKey(0)
    cv.destroyAllWindows()